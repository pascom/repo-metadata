When adding new packages, please keep the listing in alphabetical order.

If using JetBrains IDE, select the wanted lines, then in main menu: **Edit | Sort Lines**.

## openSUSE

In case you are adding a `*-devel` package, for example, `qt6-pdf-devel`, prefer adding it
as a virtual package. Run the command:
```bash
sudo zypper info --provides qt6-pdf-devel
```

You will see output like this:
```
Information for package qt6-pdf-devel:
--------------------------------------
Repository     : openSUSE-Tumbleweed-Oss
Name           : qt6-pdf-devel
Version        : 6.7.0-1.1
Arch           : x86_64
Vendor         : openSUSE
Installed Size : 94,0 KiB
Installed      : No
Status         : not installed
Source package : qt6-webengine-6.7.0-1.1.src
Upstream URL   : https://www.qt.io
Summary        : Development files for the Qt 6 Pdf library
Description    : 
    Development files for the Qt 6 Pdf library.
Provides       : [4]
    cmake(Qt6Pdf) = 6.7.0
    pkgconfig(Qt6Pdf) = 6.7.0
    qt6-pdf-devel = 6.7.0-1.1
    qt6-pdf-devel(x86-64) = 6.7.0-1.1
```

Notice that `cmake(Qt6Pdf)` and `pkgconfig(Qt6Pdf)` virtual packages.

So instead of adding `qt6-pdf-devel`, add `cmake(Qt6Pdf)`.

`cmake(xxx)` is for packages providing CMake config files (minus private-devel packages for Qt)  
`pkgconfig(xxx)` is for packages only providing pkgconfig files.  
There is a benefit for these: if a package gets renamed, the cmake / pkgconfig names would not change.  
